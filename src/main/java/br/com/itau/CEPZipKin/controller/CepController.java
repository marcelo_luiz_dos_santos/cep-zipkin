package br.com.itau.CEPZipKin.controller;

import br.com.itau.CEPZipKin.Client.Cep;
import br.com.itau.CEPZipKin.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/CEP")
public class CepController {

    @Autowired
    private CepService cepService;

    @GetMapping("/{cep}")
    public Cep getByCep(@PathVariable String cep) {
        return cepService.getByCep(cep);
    }

}
